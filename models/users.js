const mongoose = require('mongoose');

const courseSchema = new mongoose.Schema({

	firstName: {

		type:String,
		required : [true,"First Name is required"]
		
	},

lastName: {

		type:String,
		required : [true,"Last Name is required"]
		
	},


	email: {

		type:String,
		required : [true,"Email is required"]
		
	},


	password: {

		type:String,
		required : [true,"Password is required"]

	},


	isAdmin: {

		type:String,
		required : [true,"Authentication is required"]
	
	},


	mobileNo: {

		type:String,
		required : [true,"Mobile number is required"]
		
	},


	enrollments: [
	{

		required : [true,"Course Enrolled is required"]

	}


	],

	

	courseId: {

		type:String,
		required : [true,"Course ID is required"]

	},


	enrolledOn: {

		type:String,
		default: new date  
		required : [true,"Enroll date is required"]

	},

	status: {

		type:String,
		default : "Enrolled"
		required : [true,"Status is required"]
	},

})

module.exports = mongoose.model("Course", courseSchema)