const express = require('express');
const mongoose = require('mongoose');
const app = express();

mongoose.connect("mongodb+srv://admin:<password>@zuitt-bootcamp.mv91k.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true

})


mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas'));


app.use(cors());
app.use(express.json());
app.use(express.urlenoded({extended:true}));



